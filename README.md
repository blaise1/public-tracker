# STATUS : WIP README.MD FILE

## Background


GitLab is a [Google Cloud Technology Partner](https://cloud.withgoogle.com/partners/detail/?id=ACkb94YZx3RNPmFySgoxPky28lnTU6sZCUYy1ZporLF7kENvUtcB6VbmyTxnO20PGhS56KHudsYM&hl=en) with a presence in [GCP marketplace](https://console.cloud.google.com/marketplace/details/gitlab-public/gitlab)  for easy deployment. We are also one of [Google’s SaaS customers](https://cloud.google.com/customers/gitlab/) where we host our service on GCP which qualifies us under their [SaaS Sales Alignment Program](https://cloud.google.com/partners/become-a-partner/saas-initiative) (Specializing in Application Development sub category) to help organize use modern tools to build scalable cloud native solutions quickly and iteratively. 

This project aims to be the top level home of all relevant Google Cloud related activities and ongoing tasks that can be shared with the public. 
  

## Process

#### Need to make an edit to this file?

Ideally open a MR or raise an issue and tag @mayanktahil.

#### Are you an organization using both Google Cloud and/or GitLab and want to provide feed back on the partnership?
Open a new issue with context, background and feedback. Be sure to reference any other [project issues](https://gitlab.com/gitlab-org/gitlab-ce/issues) or issue to this project and assign to @mayanktahil.

> Please state the explicit ask how Alliances can help and label with `Alliance - Sales || GTM` for sales related issues or `Partner Marketing` for marketing related issues. Please label all issues with `Alliances - Google` regardless.

## Stakeholders


| **Name** | **Title** | **Responsible for**: |
| :------ | :------ | :------ |
[Mayank Tahilramani](@mayanktahil) | Sr. Alliance Manager | Google Relationship & Strategy |
[Brandon Jung](@bjung) | VP, Alliances | Partnerships Strategy |
[Patrick Deuley](@deuley) | Sr. PM, Ecosystem | Ecosystem Product Manager |
[Daniel Gruesso](@daniel) | Sr. PM, Configure | Configure Product Manager |
[Joshua Lambard Deuley](@joshlambert) | Sr. PM, Group Product Management | Verify, Enablement, etc | 
[Tina Sturgis](@TinaS) | Sr. Partner Marketing Manager | Partner Marketing Initatives 

### Integrations
  
| **Partner Product** | **GitLab Feature** | **Target Launch** |
| :------: | :------ | :------ |
| To be populated | To be populated | To be populated |
  

## Existing Collateral

Below you will find collateral and content (digital web pages, Google Docs, PDF, etc.) where you can bring yourself up to speed on our messaging, technical capabilities, and existing information relevant to our joint solution and partnership.

### Dedicated Pages

- [Google Cloud Landing Page](https://about.gitlab.com/solutions/google-cloud-platform/) (about.gitlab.com)

- [GitLab Marketplace Listing](https://console.cloud.google.com/marketplace/details/gitlab-public/gitlab) (console.cloud.google.com)

- [GitLab Partner Page](https://cloud.withgoogle.com/partners/detail/?id=ACkb94YZx3RNPmFySgoxPky28lnTU6sZCUYy1ZporLF7kENvUtcB6VbmyTxnO20PGhS56KHudsYM&hl=en) (cloud.withgoogle.com)

- [Google Cloud Build vs GitLab](https://about.gitlab.com/devops-tools/cloudbuild-vs-gitlab.html) (about.gitlab.com)

- [Scalable app deployment with GitLab and Google Cloud Platform](https://about.gitlab.com/webcast/scalable-app-deploy/) (about.gitlab.com/webcast)

- [GitLab Serverless](https://about.gitlab.com/product/serverless/) (Knative - about.gitlab.com)

- [GitLab and Google Cloud collaborate to simplify scalable app deployment](https://about.gitlab.com/press/releases/2018-04-05-gitlab-google-kubernetes-engine-integration.html) (GitLab PR)

- [Application Deployment Targets](https://about.gitlab.com/product/deploy-targets/) (about.gitlab.com/products)

### Blogs

#### about.gitlab.com/blog

 - [April 5, 2018]  [GitLab + Google Cloud Platform = simplified, scalable deployment](https://about.gitlab.com/2018/04/05/gke-gitlab-integration/)

- [April, 2018]  [Google Cloud NEXT’18 Recap](https://about.gitlab.com/2018/07/27/google-next-2018-recap/)

- [May 2, 2019]  [GitLab’s journey from Azure to GCP](https://about.gitlab.com/2019/05/02/gitlab-journey-from-azure-to-gcp/)

- [June 25, 2018]  [We’re moving from Azure to Google Cloud Platform](https://about.gitlab.com/2018/06/25/moving-to-gcp/)

- [April 25, 2018]  [GitLab + Google Cloud Platform = simplified, scalable deployment](https://about.gitlab.com/2018/04/05/gke-gitlab-integration/)

- [April 4, 2019]  [What to check out at Google Cloud Next 2019](https://about.gitlab.com/2019/04/04/google-next-post/)

- [April 1, 2019]  [The evolution of Zero Trust](https://about.gitlab.com/2019/04/01/evolution-of-zero-trust/)

- [July 18, 2019]  [Install GitLab with a single click from the new GCP Marketplace](https://about.gitlab.com/2018/07/18/install-gitlab-one-click-gcp-marketplace/)

- [April 16, 2019]  [Google Cloud Next: Doubling down on Kubernetes and multi-cloud](https://about.gitlab.com/2019/04/16/google-cloud-next-anthos-kubernetes/)

  

#### cloud.google.com/blog

  

- [April 9, 2019]  [Announcing Cloud Run, the newest member of our serverless compute stack](https://cloud.google.com/blog/products/serverless/announcing-cloud-run-the-newest-member-of-our-serverless-compute-stack)

- [April, 9, 2019]  [Introducing Anthos: An entirely new platform for managing applications in today's multi-cloud world](https://cloud.google.com/blog/topics/hybrid-cloud/new-platform-for-managing-applications-in-todays-multi-cloud-world)

- [April 5, 2019]  [Now, you can deploy to Kubernetes Engine from GitLab with a few clicks](https://cloud.google.com/blog/products/gcp/now-you-can-deploy-to-kubernetes-engine-from-gitlab-with-a-few-clicks)

## Customer Case Studies

- [about.gitlab.com] [Multi Cloud, knative, Kubernetes](https://about.gitlab.com/customers/anwb/)

## Technical Enablement

  

### First Party Content

  

#### GitLab

  

- [Docs]  [Connecting GitLab with a Kubernetes Cluster](https://docs.gitlab.com/ee/user/project/clusters/index.html#adding-an-existing-kubernetes-cluster)

- [Docs]  [Google Secure LDAP](https://docs.gitlab.com/ee/administration/auth/ldap.html#google-secure-ldap-core-only) (Configuration with GCP)

- [YouTube]  [GitLab Serverless Walkthrough](https://www.youtube.com/watch?v=IIM8JWhAbNk&feature=youtu.be)

- [CE Issue]  [GitLab Geo Deployment Storage and Network considerations in GCP](https://gitlab.com/gitlab-org/gitlab-ee/issues/12188#note_185910256)

- [YouTube - OLD]  [Installing GitLab on Google Container Engine (GKE)](https://www.youtube.com/watch?v=HLNNFS8b_aw)

  

#### Google Cloud

  

- [Solutions Paper]  [Deploying production-ready GitLab on Google Kubernetes Engine](https://cloud.google.com/solutions/deploying-production-ready-gitlab-on-gke)

- [Qwiklabs]  [Introduction to GitLab on GKE](https://www.qwiklabs.com/focuses/4815?catalog_rank=%7B%22rank%22%3A1%2C%22num_filters%22%3A0%2C%22has_search%22%3Atrue%7D&parent=catalog&search_id=2843491)

- [GitHub]  [terraform-google-modules/terraform-google-gke-gitlab](https://github.com/terraform-google-modules/terraform-google-gke-gitlab)

- [Solutions Paper]  [Mirroring GitLab repositories to Cloud Source Repositories](https://cloud.google.com/solutions/mirroring-gitlab-repositories-to-cloud-source-repositories)

### External Content

- [Medium.com July 3, 2018]  [How to set up Gitlab CI/CD with Google Cloud Container Registry and Kubernetes](https://medium.com/@davivc/how-to-set-up-gitlab-ci-cd-with-google-cloud-container-registry-and-kubernetes-fa88ab7b1295)

- [Webinar DevOps.com]  [Running Containerized Applications on Modern Serverless Platforms](https://drive.google.com/file/d/1K5lW5IzTvEOIJpLBIKi_vCdICeSv-3VI/view)

- [Medium.com June 13, 2018]  [How to set up GitLab Single Sign-On with Google G Suite](https://medium.com/mop-developers/how-to-set-up-gitlab-single-sign-on-with-google-g-suite-f5e88ae8ba7)

- [Medium November 4 2018]  [Automatically deploy to Google App Engine with Gitlab CI](https://medium.com/google-cloud/automatically-deploy-to-google-app-engine-with-gitlab-ci-d1c7237cbe11)

- [dev.to January 2019]  [Go, Google Functions and Gitlab-ci a perfect combination](https://dev.to/renatosuero/go-google-functions-and-gitlab-ci-a-perfect-combination-4lao)

- [GitHub]  [Deploy Google Cloud Functions: GitLab CI/CD Pipeline Config File](https://gist.github.com/troyharvey/bae82c86c27a3aa539dea83857ee9ecd)

- [Medium.com September 7 2018]  [Deploying to Google Kubernetes Engine from Gitlab CI](https://medium.com/john-lewis-software-engineering/deploying-to-google-kubernetes-engine-from-gitlab-ci-feaf51dae0c1)

- [Medium.com March 30 2019]  [Gitlab Continuous Deployment Pipeline to GKE with Helm](https://medium.com/google-cloud/gitlab-continuous-deployment-pipeline-to-gke-with-helm-69d8a15ed910)
  